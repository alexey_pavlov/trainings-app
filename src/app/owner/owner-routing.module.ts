import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// providers
import { AuthGuard } from '@core/guards/auth.guard';
import { OwnerGuard } from '@core/guards/owner.guard';
import { OwnerTrainingsResolver } from './resolvers/owner-trainings.resolver';

// components
import { OwnerTrainingsPageComponent } from './components/owner-trainings.page/owner-trainings.page.component';
import { OwnerTrainingPageComponent } from './components/owner-training.page/owner-training.page.component';
import { OwnerLessonPageComponent } from './components/owner-lesson.page/owner-lesson.page.component';

// other
import { newTrainingTempId, newLessonTempId } from './owner.constants';

const routes: Routes = [
    {
        path: 'trainings',
        component: OwnerTrainingsPageComponent,
        canActivate: [AuthGuard, OwnerGuard],
        resolve: {
            trainings: OwnerTrainingsResolver
        }
    },
    {
        path: 'trainings/:id',
        component: OwnerTrainingPageComponent,
        canActivate: [AuthGuard, OwnerGuard],
        resolve: {
            trainings: OwnerTrainingsResolver
        }
    },
    {
        path: `trainings/${newTrainingTempId}`,
        component: OwnerTrainingPageComponent,
        canActivate: [AuthGuard, OwnerGuard]
    },
    {
        path: `trainings/:trainingId/lessons/:id`,
        component: OwnerLessonPageComponent,
        canActivate: [AuthGuard, OwnerGuard],
        resolve: {
            trainings: OwnerTrainingsResolver
        }
    },
    {
        path: `trainings/:trainingId/lessons/${newLessonTempId}`,
        component: OwnerLessonPageComponent,
        canActivate: [AuthGuard, OwnerGuard],
        resolve: {
            trainings: OwnerTrainingsResolver
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class OwnerRoutingModule {}

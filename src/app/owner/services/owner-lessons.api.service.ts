import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IOwnerLesson } from '../interfaces';

@Injectable()
export class OwnerLessonsApiService {
    constructor(private http: HttpClient) {}

    createLesson(lesson: IOwnerLesson): Observable<IOwnerLesson> {
        return this.http.post<IOwnerLesson>('/owner/lessons', lesson)
            .map(result => result as IOwnerLesson);
    }

    updateLesson(lesson: IOwnerLesson): Observable<IOwnerLesson> {
        return this.http.put<IOwnerLesson>('/owner/lessons/' + lesson.id, lesson)
            .map(result => result as IOwnerLesson);
    }

    deleteDelete(lesson: IOwnerLesson): Observable<any> {
        return this.http.delete('/owner/lessons/' + lesson.id);
    }
}

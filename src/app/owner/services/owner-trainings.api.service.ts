import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IOwnerTraining } from '../interfaces';

@Injectable()
export class OwnerTrainingsApiService {
    constructor(private http: HttpClient) {}

    loadTrainings(): Observable<IOwnerTraining[]> {
        return this.http.get<IOwnerTraining[]>('/owner/trainings')
            .map(result => result as IOwnerTraining[]);
    }

    createTraining(training: IOwnerTraining): Observable<IOwnerTraining> {
        return this.http.post<IOwnerTraining>('/owner/trainings', training)
            .map(result => result as IOwnerTraining);
    }

    updateTraining(training: IOwnerTraining): Observable<IOwnerTraining> {
        return this.http.put<IOwnerTraining>('/owner/trainings/' + training.id, training)
            .map(result => result as IOwnerTraining);
    }

    deleteDelete(training: IOwnerTraining): Observable<any> {
        return this.http.delete('/owner/trainings/' + training.id);
    }
}

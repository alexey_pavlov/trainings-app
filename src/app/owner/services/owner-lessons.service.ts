import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';

import { IOwnerLesson, IOwnerTraining } from '../interfaces';
import * as fromOwnerLessonsActions from '../state/actions/owner-lessons.actions';
import { OwnerTrainingsService } from './owner-trainings.service';

@Injectable()
export class OwnerLessonsService {
    ownerLessonCreatedSuccessfully$: Observable<IOwnerTraining> = this.actions$
        .ofType(fromOwnerLessonsActions.ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON_SUCCESS)
        .map((action: fromOwnerLessonsActions.OwnerLessonsCreateOwnerLessonSuccess) => action.payload);

    ownerLessonCreatedFailure$: Observable<HttpErrorResponse> = this.actions$
        .ofType(fromOwnerLessonsActions.ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON_FAIL)
        .map((action: fromOwnerLessonsActions.OwnerLessonsCreateOwnerLessonFail) => action.payload);

    constructor(
        private store: Store<any>,
        private actions$: Actions,
        private ownerTrainingsService: OwnerTrainingsService
    ) {}

    createOwnerLesson(lesson: IOwnerLesson) {
        this.store.dispatch(new fromOwnerLessonsActions.OwnerLessonsCreateOwnerLesson(lesson));
    }

    updateOwnerLesson(lesson: IOwnerLesson) {
        this.store.dispatch(new fromOwnerLessonsActions.OwnerLessonsUpdateOwnerLesson(lesson));
    }

    deleteOwnerLesson(lesson: IOwnerLesson) {
        this.store.dispatch(new fromOwnerLessonsActions.OwnerLessonsDeleteOwnerLesson(lesson));
    }

    getOwnerLessonByIdAndTrainingId({ id, trainingId }: Partial<IOwnerLesson>): Observable<IOwnerLesson> {
        return this.ownerTrainingsService.ownerTrainingsAfterLoading$
            .map((trainings: IOwnerTraining[]) => trainings.find(training => training.id === trainingId))
            .map((training: IOwnerTraining) => training.lessons.find(lesson => lesson.id === id));
    }
}

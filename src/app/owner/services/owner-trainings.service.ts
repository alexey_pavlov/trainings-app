import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { Actions } from '@ngrx/effects';
import { isEmpty } from 'lodash';

import { IOwnerTraining } from '../interfaces';
import * as fromOwnerTrainingsActions from '../state/actions/owner-trainings.actions';
import * as fromOwnerTrainingsState from '../state';

@Injectable()
export class OwnerTrainingsService {
    ownerTrainings$: Store<IOwnerTraining[]> = this.store.select(fromOwnerTrainingsState.getOwnerTrainingsList);
    ownerTrainingsAreLoaded$: Observable<boolean> = this.store.select(fromOwnerTrainingsState.getOwnerTrainingsAreLoaded);
    ownerTrainingsAreLoading$: Observable<boolean> = this.store.select(fromOwnerTrainingsState.getOwnerTrainingsAreLoading);
    ownerTrainingsAfterLoading$: Observable<IOwnerTraining[]>;
    thereAreNoOwnerTrainings$: Observable<boolean>;

    ownerTrainingCreatedSuccessfully$: Observable<IOwnerTraining> = this.actions$
        .ofType(fromOwnerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING_SUCCESS)
        .map((action: fromOwnerTrainingsActions.OwnerTrainingsCreateOwnerTrainingSuccess) => action.payload);

    ownerTrainingCreatedFailure$: Observable<HttpErrorResponse> = this.actions$
        .ofType(fromOwnerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING_FAIL)
        .map((action: fromOwnerTrainingsActions.OwnerTrainingsCreateOwnerTrainingFail) => action.payload);

    constructor(
        private store: Store<any>,
        private actions$: Actions
    ) {
        this.ownerTrainingsAfterLoading$ = this.ownerTrainingsAreLoaded$
            .filter(loaded => loaded)
            .switchMapTo(this.ownerTrainings$);

        this.thereAreNoOwnerTrainings$ = this.ownerTrainings$.map((trainings: IOwnerTraining[]) => isEmpty(trainings));
    }

    loadOwnerTrainings() {
        this.store.dispatch(new fromOwnerTrainingsActions.OwnerTrainingsLoadOwnerTrainings());
    }

    createOwnerTraining(training: IOwnerTraining) {
        this.store.dispatch(new fromOwnerTrainingsActions.OwnerTrainingsCreateOwnerTraining(training));
    }

    updateOwnerTraining(training: IOwnerTraining) {
        this.store.dispatch(new fromOwnerTrainingsActions.OwnerTrainingsUpdateOwnerTraining(training));
    }

    deleteOwnerTraining(training: IOwnerTraining) {
        this.store.dispatch(new fromOwnerTrainingsActions.OwnerTrainingsDeleteOwnerTraining(training));
    }

    getOwnerTrainingById(id: string): Observable<IOwnerTraining> {
        return this.ownerTrainingsAfterLoading$
            .map((trainings: IOwnerTraining[]) => trainings.find(training => training.id === id));
    }
}

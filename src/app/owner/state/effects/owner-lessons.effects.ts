import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import * as ownerLessonsActions from '../actions/owner-lessons.actions';
import { IOwnerLesson } from '../../interfaces';
import { OwnerLessonsApiService } from '../../services/owner-lessons.api.service';

@Injectable()
export class OwnerLessonsEffects {
    constructor(
        private actions$: Actions,
        private ownerLessonsApiService: OwnerLessonsApiService
    ) {}

    @Effect()
    createOwnerLesson$: Observable<Action> = this.actions$
        .ofType(ownerLessonsActions.ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON)
        .map((action: ownerLessonsActions.OwnerLessonsCreateOwnerLesson) => action.payload)
        .switchMap((lesson: IOwnerLesson) => {
            return this.ownerLessonsApiService.createLesson(lesson)
                .map((createdLesson: IOwnerLesson) => new ownerLessonsActions.OwnerLessonsCreateOwnerLessonSuccess(createdLesson))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerLessonsActions.OwnerLessonsCreateOwnerLessonFail(error)));
        });

    @Effect()
    updateOwnerLesson$: Observable<Action> = this.actions$
        .ofType(ownerLessonsActions.ownerLessonsActionTypes.OWNER_LESSONS_UPDATE_OWNER_LESSON)
        .map((action: ownerLessonsActions.OwnerLessonsUpdateOwnerLesson) => action.payload)
        .switchMap((lesson: IOwnerLesson) => {
            return this.ownerLessonsApiService.updateLesson(lesson)
                .map((updatedLesson: IOwnerLesson) => new ownerLessonsActions.OwnerLessonsUpdateOwnerLessonSuccess(updatedLesson))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerLessonsActions.OwnerLessonsUpdateOwnerLessonFail(error)));
        });

    @Effect()
    deleteOwnerLesson$: Observable<Action> = this.actions$
        .ofType(ownerLessonsActions.ownerLessonsActionTypes.OWNER_LESSONS_DELETE_OWNER_LESSON)
        .map((action: ownerLessonsActions.OwnerLessonsDeleteOwnerLesson) => action.payload)
        .switchMap((lesson: IOwnerLesson) => {
            return this.ownerLessonsApiService.deleteDelete(lesson)
                .map(() => new ownerLessonsActions.OwnerLessonsDeleteOwnerLessonSuccess(lesson))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerLessonsActions.OwnerLessonsDeleteOwnerLessonFail(error)));
        });
}

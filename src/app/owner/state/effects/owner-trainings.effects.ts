import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';
import { Actions, Effect } from '@ngrx/effects';

import * as ownerTrainingsActions from '../actions/owner-trainings.actions';
import { IOwnerTraining } from '../../interfaces';
import { OwnerTrainingsApiService } from '../../services/owner-trainings.api.service';
import { OwnerTrainingsService } from '../../services/owner-trainings.service';
import * as fromAuthActions from '@core/state/actions/auth.actions';

@Injectable()
export class OwnerTrainingsEffects {
    constructor(
        private ownerTrainingsApiService: OwnerTrainingsApiService,
        private ownerTrainingsService: OwnerTrainingsService,
        private actions$: Actions
    ) {}

    @Effect()
    loadOwnerTrainings$: Observable<Action> = this.actions$
        .ofType(ownerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS)
        .switchMap(() => {
            return this.ownerTrainingsService.ownerTrainingsAreLoaded$.take(1)
                .switchMap((trainingsAreLoaded: boolean) => {
                    if (!trainingsAreLoaded) {
                        return this.ownerTrainingsApiService.loadTrainings()
                            .map((trainings: IOwnerTraining[]) => new ownerTrainingsActions.OwnerTrainingsLoadOwnerTrainingsSuccess(trainings))
                            .catch((error: HttpErrorResponse) => Observable.of(new ownerTrainingsActions.OwnerTrainingsLoadOwnerTrainingsFail(error)));
                    } else {
                        return Observable.never();
                    }
                });
        });

    @Effect()
    createOwnerTraining$: Observable<Action> = this.actions$
        .ofType(ownerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING)
        .map((action: ownerTrainingsActions.OwnerTrainingsCreateOwnerTraining) => action.payload)
        .switchMap((training: IOwnerTraining) => {
            return this.ownerTrainingsApiService.createTraining(training)
                .map((createdTraining: IOwnerTraining) => new ownerTrainingsActions.OwnerTrainingsCreateOwnerTrainingSuccess(createdTraining))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerTrainingsActions.OwnerTrainingsCreateOwnerTrainingFail(error)));
        });

    @Effect()
    updateOwnerTraining$: Observable<Action> = this.actions$
        .ofType(ownerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_UPDATE_OWNER_TRAINING)
        .map((action: ownerTrainingsActions.OwnerTrainingsUpdateOwnerTraining) => action.payload)
        .switchMap((training: IOwnerTraining) => {
            return this.ownerTrainingsApiService.updateTraining(training)
                .map((updatedTraining: IOwnerTraining) => new ownerTrainingsActions.OwnerTrainingsUpdateOwnerTrainingSuccess(updatedTraining))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerTrainingsActions.OwnerTrainingsUpdateOwnerTrainingFail(error)));
        });

    @Effect()
    deleteOwnerTraining$: Observable<Action> = this.actions$
        .ofType(ownerTrainingsActions.ownerTrainingsActionTypes.OWNER_TRAININGS_DELETE_OWNER_TRAINING)
        .map((action: ownerTrainingsActions.OwnerTrainingsDeleteOwnerTraining) => action.payload)
        .switchMap((training: IOwnerTraining) => {
            return this.ownerTrainingsApiService.deleteDelete(training)
                .map(() => new ownerTrainingsActions.OwnerTrainingsDeleteOwnerTrainingSuccess(training))
                .catch((error: HttpErrorResponse) => Observable.of(new ownerTrainingsActions.OwnerTrainingsDeleteOwnerTrainingFail(error)));
        });

    @Effect() logout$: Observable<Action> = this.actions$
        .ofType(fromAuthActions.authActionTypes.AUTH_RESET)
        .map(() => new ownerTrainingsActions.OwnerTrainingsReset());
}

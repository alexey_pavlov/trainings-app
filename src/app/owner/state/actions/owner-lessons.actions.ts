import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { typeCacheUtil } from '@shared/utilities/type-cache.util';
import { IOwnerLesson } from '../../interfaces';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'typeCacheUtil' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ownerLessonsActionTypes = {
    OWNER_LESSONS_LOAD_MORE_OWNER_LESSONS: typeCacheUtil('[Owner lessons] Load more ownerLessons'),
    OWNER_LESSONS_LOAD_MORE_OWNER_LESSONS_SUCCESS: typeCacheUtil('[Owner lessons] Load more ownerLessons success'),
    OWNER_LESSONS_LOAD_MORE_OWNER_LESSONS_FAIL: typeCacheUtil('[Owner lessons] Load more ownerLessons fail'),
    OWNER_LESSONS_CREATE_OWNER_LESSON: typeCacheUtil('[Owner lessons] Create owner lesson'),
    OWNER_LESSONS_CREATE_OWNER_LESSON_SUCCESS: typeCacheUtil('[Owner lessons] Create owner lesson success'),
    OWNER_LESSONS_CREATE_OWNER_LESSON_FAIL: typeCacheUtil('[Owner lessons] Create owner lesson fail'),
    OWNER_LESSONS_UPDATE_OWNER_LESSON: typeCacheUtil('[Owner lessons] Update owner lesson'),
    OWNER_LESSONS_UPDATE_OWNER_LESSON_SUCCESS: typeCacheUtil('[Owner lessons] Update owner lesson success'),
    OWNER_LESSONS_UPDATE_OWNER_LESSON_FAIL: typeCacheUtil('[Owner lessons] Update owner lesson fail'),
    OWNER_LESSONS_DELETE_OWNER_LESSON: typeCacheUtil('[Owner lessons] Delete owner lesson'),
    OWNER_LESSONS_DELETE_OWNER_LESSON_SUCCESS: typeCacheUtil('[Owner lessons] Delete owner lesson success'),
    OWNER_LESSONS_DELETE_OWNER_LESSON_FAIL: typeCacheUtil('[Owner lessons] Delete owner lesson fail'),
    OWNER_LESSONS_RESET: typeCacheUtil('[Owner lessons] Reset')
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class OwnerLessonsCreateOwnerLesson implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsCreateOwnerLessonSuccess implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON_SUCCESS;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsCreateOwnerLessonFail implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerLessonsUpdateOwnerLesson implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_UPDATE_OWNER_LESSON;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsUpdateOwnerLessonSuccess implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_UPDATE_OWNER_LESSON_SUCCESS;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsUpdateOwnerLessonFail implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_UPDATE_OWNER_LESSON_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerLessonsDeleteOwnerLesson implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_DELETE_OWNER_LESSON;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsDeleteOwnerLessonSuccess implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_DELETE_OWNER_LESSON_SUCCESS;

    constructor(public payload: IOwnerLesson) {}
}

export class OwnerLessonsDeleteOwnerLessonFail implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_DELETE_OWNER_LESSON_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerLessonsReset implements Action {
    readonly type = ownerLessonsActionTypes.OWNER_LESSONS_RESET;
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type OwnerLessonsActions = [
    OwnerLessonsUpdateOwnerLesson,
    OwnerLessonsUpdateOwnerLessonSuccess,
    OwnerLessonsUpdateOwnerLessonFail,
    OwnerLessonsCreateOwnerLesson,
    OwnerLessonsCreateOwnerLessonSuccess,
    OwnerLessonsCreateOwnerLessonFail,
    OwnerLessonsDeleteOwnerLesson,
    OwnerLessonsDeleteOwnerLessonSuccess,
    OwnerLessonsDeleteOwnerLessonFail,
    OwnerLessonsReset
];

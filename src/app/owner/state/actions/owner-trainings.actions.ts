import { HttpErrorResponse } from '@angular/common/http';
import { Action } from '@ngrx/store';

import { typeCacheUtil } from '@shared/utilities/type-cache.util';
import { IOwnerTraining } from '../../interfaces';

/**
 * For each action type in an action group, make a simple
 * enum object for all of this group's action types.
 *
 * The 'typeCacheUtil' utility function coerces strings into string
 * literal types and runs a simple check to guarantee all
 * action types in the application are unique.
 */
export const ownerTrainingsActionTypes = {
    OWNER_TRAININGS_LOAD_OWNER_TRAININGS: typeCacheUtil('[Owner trainings] Load ownerTrainings'),
    OWNER_TRAININGS_LOAD_OWNER_TRAININGS_SUCCESS: typeCacheUtil('[Owner trainings] Load ownerTrainings success'),
    OWNER_TRAININGS_LOAD_OWNER_TRAININGS_FAIL: typeCacheUtil('[Owner trainings] Load ownerTrainings fail'),
    OWNER_TRAININGS_LOAD_MORE_OWNER_TRAININGS: typeCacheUtil('[Owner trainings] Load more ownerTrainings'),
    OWNER_TRAININGS_LOAD_MORE_OWNER_TRAININGS_SUCCESS: typeCacheUtil('[Owner trainings] Load more ownerTrainings success'),
    OWNER_TRAININGS_LOAD_MORE_OWNER_TRAININGS_FAIL: typeCacheUtil('[Owner trainings] Load more ownerTrainings fail'),
    OWNER_TRAININGS_CREATE_OWNER_TRAINING: typeCacheUtil('[Owner trainings] Create owner training'),
    OWNER_TRAININGS_CREATE_OWNER_TRAINING_SUCCESS: typeCacheUtil('[Owner trainings] Create owner training success'),
    OWNER_TRAININGS_CREATE_OWNER_TRAINING_FAIL: typeCacheUtil('[Owner trainings] Create owner training fail'),
    OWNER_TRAININGS_UPDATE_OWNER_TRAINING: typeCacheUtil('[Owner trainings] Update owner training'),
    OWNER_TRAININGS_UPDATE_OWNER_TRAINING_SUCCESS: typeCacheUtil('[Owner trainings] Update owner training success'),
    OWNER_TRAININGS_UPDATE_OWNER_TRAINING_FAIL: typeCacheUtil('[Owner trainings] Update owner training fail'),
    OWNER_TRAININGS_DELETE_OWNER_TRAINING: typeCacheUtil('[Owner trainings] Delete owner training'),
    OWNER_TRAININGS_DELETE_OWNER_TRAINING_SUCCESS: typeCacheUtil('[Owner trainings] Delete owner training success'),
    OWNER_TRAININGS_DELETE_OWNER_TRAINING_FAIL: typeCacheUtil('[Owner trainings] Delete owner training fail'),
    OWNER_TRAININGS_RESET: typeCacheUtil('[Owner trainings] Reset')
};

/**
 * Every action is comprised of at least a type and an optional
 * payload. Expressing actions as classes enables powerful
 * type checking in reducer functions.
 *
 * See Discriminated Unions: https://www.typescriptlang.org/docs/handbook/advanced-types.html#discriminated-unions
 */

export class OwnerTrainingsLoadOwnerTrainings implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS;
}

export class OwnerTrainingsLoadOwnerTrainingsSuccess implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS_SUCCESS;

    constructor(public payload: IOwnerTraining[]) {}
}

export class OwnerTrainingsLoadOwnerTrainingsFail implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerTrainingsCreateOwnerTraining implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsCreateOwnerTrainingSuccess implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING_SUCCESS;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsCreateOwnerTrainingFail implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerTrainingsUpdateOwnerTraining implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_UPDATE_OWNER_TRAINING;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsUpdateOwnerTrainingSuccess implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_UPDATE_OWNER_TRAINING_SUCCESS;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsUpdateOwnerTrainingFail implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_UPDATE_OWNER_TRAINING_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerTrainingsDeleteOwnerTraining implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_DELETE_OWNER_TRAINING;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsDeleteOwnerTrainingSuccess implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_DELETE_OWNER_TRAINING_SUCCESS;

    constructor(public payload: IOwnerTraining) {}
}

export class OwnerTrainingsDeleteOwnerTrainingFail implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_DELETE_OWNER_TRAINING_FAIL;

    constructor(public payload: HttpErrorResponse) {}
}

export class OwnerTrainingsReset implements Action {
    readonly type = ownerTrainingsActionTypes.OWNER_TRAININGS_RESET;
}

/**
 * Export a type alias of all actions in this action group
 * so that reducers can easily compose action types
 */
export type OwnerTrainingsActions = [
    OwnerTrainingsLoadOwnerTrainings,
    OwnerTrainingsLoadOwnerTrainingsSuccess,
    OwnerTrainingsLoadOwnerTrainingsFail,
    OwnerTrainingsUpdateOwnerTraining,
    OwnerTrainingsUpdateOwnerTrainingSuccess,
    OwnerTrainingsUpdateOwnerTrainingFail,
    OwnerTrainingsCreateOwnerTraining,
    OwnerTrainingsCreateOwnerTrainingSuccess,
    OwnerTrainingsCreateOwnerTrainingFail,
    OwnerTrainingsDeleteOwnerTraining,
    OwnerTrainingsDeleteOwnerTrainingSuccess,
    OwnerTrainingsDeleteOwnerTrainingFail,
    OwnerTrainingsReset
];

import { ownerTrainingsActionTypes } from '../actions/owner-trainings.actions';
import { ownerLessonsActionTypes } from '../actions/owner-lessons.actions';
import { IOwnerTraining } from '../../interfaces';

export interface IOwnerTrainingsState {
    ownerTrainingsList: IOwnerTraining[];
    ownerTrainingsAreLoaded: boolean;
    ownerTrainingsAreLoading: boolean;
}

export const initialOwnerTrainingsStoreState: IOwnerTrainingsState = {
    ownerTrainingsList: [],
    ownerTrainingsAreLoaded: false,
    ownerTrainingsAreLoading: false
};

export function ownerTrainingsReducer(state: IOwnerTrainingsState = initialOwnerTrainingsStoreState, action): IOwnerTrainingsState {
    switch (action.type) {
        case ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS: {
            return Object.assign({}, state, {
                ownerTrainingsAreLoading: true
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS_SUCCESS: {
            return Object.assign({}, state, {
                ownerTrainingsList: action.payload,
                ownerTrainingsAreLoaded: true,
                ownerTrainingsAreLoading: false
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_LOAD_OWNER_TRAININGS_FAIL: {
            return Object.assign({}, state, {
                ownerTrainingsAreLoaded: true,
                ownerTrainingsAreLoading: false
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_CREATE_OWNER_TRAINING_SUCCESS: {
            return Object.assign({}, state, {
                ownerTrainingsList: [
                    ...state.ownerTrainingsList,
                    Object.assign({}, action.payload)
                ]
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_UPDATE_OWNER_TRAINING_SUCCESS: {
            const trainingIndex = state.ownerTrainingsList.findIndex(training => training.id === action.payload.id);

            return Object.assign({}, state, {
                ownerTrainingsList: [
                    ...state.ownerTrainingsList.slice(0, trainingIndex),
                    Object.assign({}, action.payload),
                    ...state.ownerTrainingsList.slice(trainingIndex + 1)
                ]
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_DELETE_OWNER_TRAINING_SUCCESS: {
            return Object.assign({}, state, {
                ownerTrainingsList: state.ownerTrainingsList.filter(training => training.id !== action.payload.id)
            });
        }

        case ownerLessonsActionTypes.OWNER_LESSONS_CREATE_OWNER_LESSON_SUCCESS: {
            const trainingIndex = state.ownerTrainingsList.findIndex(training => training.id === action.payload.trainingId);
            const trainingInStore = state.ownerTrainingsList[trainingIndex];

            return Object.assign({}, state, {
                ownerTrainingsList: [
                    ...state.ownerTrainingsList.slice(0, trainingIndex),
                    Object.assign({}, trainingInStore, {
                        lessons: [...(trainingInStore.lessons || []), action.payload]
                    }),
                    ...state.ownerTrainingsList.slice(trainingIndex + 1)
                ]
            });
        }

        case ownerLessonsActionTypes.OWNER_LESSONS_UPDATE_OWNER_LESSON_SUCCESS: {
            const trainingIndex = state.ownerTrainingsList.findIndex(training => training.id === action.payload.trainingId);
            const trainingInStore = state.ownerTrainingsList[trainingIndex];
            const lessonIndex = trainingInStore.lessons.findIndex(lesson => lesson.id === action.payload.id);
            const lessonInStore = trainingInStore.lessons[lessonIndex];

            return Object.assign({}, state, {
                ownerTrainingsList: [
                    ...state.ownerTrainingsList.slice(0, trainingIndex),
                    Object.assign({}, trainingInStore, {
                        lessons: [
                            ...trainingInStore.lessons.slice(0, lessonIndex),
                            Object.assign({}, lessonInStore, action.payload),
                            ...trainingInStore.lessons.slice(lessonIndex + 1)
                        ]
                    }),
                    ...state.ownerTrainingsList.slice(trainingIndex + 1)
                ]
            });
        }

        case ownerLessonsActionTypes.OWNER_LESSONS_DELETE_OWNER_LESSON_SUCCESS: {
            const trainingIndex = state.ownerTrainingsList.findIndex(training => training.id === action.payload.trainingId);
            const trainingInStore = state.ownerTrainingsList[trainingIndex];

            return Object.assign({}, state, {
                ownerTrainingsList: [
                    ...state.ownerTrainingsList.slice(0, trainingIndex),
                    Object.assign({}, trainingInStore, {
                        lessons: trainingInStore.lessons.filter(lesson => lesson.id !== action.payload.id)
                    }),
                    ...state.ownerTrainingsList.slice(trainingIndex + 1)
                ]
            });
        }

        case ownerTrainingsActionTypes.OWNER_TRAININGS_RESET: {
            return initialOwnerTrainingsStoreState;
        }

        default: {
            return state;
        }
    }
}

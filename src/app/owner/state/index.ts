import { ActionReducerMap } from '@ngrx/store';
import { createSelector } from 'reselect';

import * as fromOwnerTrainingsReducer from './reducers/owner-trainings.reducer';

/**
 * We treat each reducer like a table in a database. This means
 * our top level state interface is just a map of keys to inner state types.
 */
export interface OwnerModuleState {
    trainings: fromOwnerTrainingsReducer.IOwnerTrainingsState;
}

export const ownerModuleInitialState: OwnerModuleState = {
    trainings: fromOwnerTrainingsReducer.initialOwnerTrainingsStoreState
};

export const reducers: ActionReducerMap<OwnerModuleState> = {
    trainings: fromOwnerTrainingsReducer.ownerTrainingsReducer
};

// main selectors
export const getOwnerTrainingsState = state => state.OwnerModule.trainings;

// selectors
export const getOwnerTrainingsList = createSelector(getOwnerTrainingsState, (state: fromOwnerTrainingsReducer.IOwnerTrainingsState) => state.ownerTrainingsList);
export const getOwnerTrainingsAreLoaded = createSelector(getOwnerTrainingsState, (state: fromOwnerTrainingsReducer.IOwnerTrainingsState) => state.ownerTrainingsAreLoaded);
export const getOwnerTrainingsAreLoading = createSelector(getOwnerTrainingsState, (state: fromOwnerTrainingsReducer.IOwnerTrainingsState) => state.ownerTrainingsAreLoading);

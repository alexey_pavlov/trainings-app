import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

import { IOwnerLesson } from '../../interfaces';
import { newLessonTempId } from '../../owner.constants';

@Component({
    selector: 'app-owner-lessons-list',
    templateUrl: './owner-lessons-list.component.html',
    styleUrls: ['./owner-lessons-list.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OwnerLessonsListComponent {
    @Input() lessons: IOwnerLesson[];
    @Output() deleteLesson: EventEmitter<void> = new EventEmitter<void>();

    newLessonTempId = newLessonTempId;
}

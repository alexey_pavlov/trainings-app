import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { OwnerTrainingsService } from '../../services/owner-trainings.service';
import { IOwnerTraining } from '../../interfaces';

@Component({
    selector: 'app-owner-trainings-page',
    templateUrl: './owner-trainings.page.component.html',
    styleUrls: ['./owner-trainings.page.component.scss']
})
export class OwnerTrainingsPageComponent {
    trainings$: Observable<IOwnerTraining[]> = this.ownerTrainingsService.ownerTrainings$;

    constructor(private ownerTrainingsService: OwnerTrainingsService) {}

    deleteTraining(training: IOwnerTraining) {
        this.ownerTrainingsService.deleteOwnerTraining(training);
    }
}

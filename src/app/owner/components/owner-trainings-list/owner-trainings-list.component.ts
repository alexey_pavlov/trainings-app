import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IOwnerTraining } from '../../interfaces';
import { newTrainingTempId } from '../../owner.constants';

@Component({
    selector: 'app-owner-trainings-list',
    templateUrl: './owner-trainings-list.component.html',
    styleUrls: ['./owner-trainings-list.component.scss']
})
export class OwnerTrainingsListComponent {
    @Input() trainings: IOwnerTraining[];
    @Output() deleteTraining: EventEmitter<IOwnerTraining> = new EventEmitter();

    newTrainingTempId = newTrainingTempId;
}

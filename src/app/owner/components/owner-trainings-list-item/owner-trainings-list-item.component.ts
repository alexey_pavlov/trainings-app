import { Component, EventEmitter, Input, Output } from '@angular/core';

import { IOwnerTraining } from '../../interfaces';

@Component({
    selector: 'app-owner-trainings-list-item',
    templateUrl: './owner-trainings-list-item.component.html',
    styleUrls: ['./owner-trainings-list-item.component.scss']
})
export class OwnerTrainingsListItemComponent {
    @Input() training: IOwnerTraining;
    @Output() delete: EventEmitter<IOwnerTraining> = new EventEmitter();
}

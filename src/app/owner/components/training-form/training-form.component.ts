import { Component, ViewEncapsulation, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { get, isEmpty } from 'lodash';

import { IOwnerTraining } from '../../interfaces';

@Component({
    selector: 'app-training-form',
    templateUrl: './training-form.component.html',
    styleUrls: ['./training-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class TrainingFormComponent implements OnInit {
    @Input() training: IOwnerTraining;
    @Output() save: EventEmitter<IOwnerTraining> = new EventEmitter();

    form: FormGroup;
    formSubmitted: boolean = false;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            title: [get(this.training, 'title'), Validators.compose([Validators.required, Validators.minLength(5)])],
            description: get(this.training, 'description'),
            text: get(this.training, 'text')
        });
    }

    submitForm() {
        this.formSubmitted = true;

        if (this.form.valid) {
            this.save.emit(Object.assign({}, this.training, this.form.value));
        }
    }

    get isNew(): boolean {
        return isEmpty(get(this.training, 'id'));
    }
}

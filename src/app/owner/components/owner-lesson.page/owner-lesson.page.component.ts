import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { isEmpty } from 'lodash';

import { OwnerLessonsService } from '../../services/owner-lessons.service';
import { IOwnerLesson } from '../../interfaces';
import { newLessonTempId } from '../../owner.constants';
import { IsDestroyedMixin } from '@shared/mixins/is-destroyed.mixin';

@Component({
    selector: 'app-owner-lesson-page',
    templateUrl: './owner-lesson.page.component.html',
    styleUrls: ['./owner-lesson.page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OwnerLessonPageComponent extends IsDestroyedMixin implements OnInit {
    lesson$: Observable<IOwnerLesson>;
    trainingId: string;

    constructor(
        private ownerLessonsService: OwnerLessonsService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        super();
    }

    ngOnInit() {
        this.lesson$ = this.activatedRoute.params
            .switchMap(({ trainingId, id }: { trainingId: string; id: string; }) => {
                this.trainingId = trainingId;

                if (!isEmpty(id) && id !== newLessonTempId) {
                    return this.ownerLessonsService.getOwnerLessonByIdAndTrainingId({ id, trainingId });
                } else {
                    return Observable.of(null);
                }
            })
            .takeUntil(this.itIsDestroyed);
    }

    save(lesson: IOwnerLesson) {
        if (!lesson.id) {
            this.ownerLessonsService.ownerLessonCreatedSuccessfully$
                .take(1)
                .takeUntil(this.itIsDestroyed)
                .takeUntil(this.ownerLessonsService.ownerLessonCreatedFailure$)
                .subscribe((createdLesson: IOwnerLesson) => {
                    this.router.navigate(['owner/trainings', this.trainingId, 'lessons', createdLesson.id]);
                });

            this.ownerLessonsService.createOwnerLesson(Object.assign({}, lesson, {
                trainingId: this.trainingId
            }));
        } else {
            this.ownerLessonsService.updateOwnerLesson(lesson);
        }
    }
}

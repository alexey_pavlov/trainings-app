import { Input, Output, EventEmitter, Component, ViewEncapsulation } from '@angular/core';

import { IOwnerLesson } from '../../interfaces';

@Component({
    selector: 'app-owner-lessons-list-item',
    templateUrl: './owner-lessons-list-item.component.html',
    styleUrls: ['./owner-lessons-list-item.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OwnerLessonsListItemComponent {
    @Input() lesson: IOwnerLesson;
    @Output() delete: EventEmitter<void> = new EventEmitter<void>();
}

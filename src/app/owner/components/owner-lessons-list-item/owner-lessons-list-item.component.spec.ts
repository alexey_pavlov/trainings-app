import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnerLessonsListItemComponent } from './owner-lessons-list-item.component';

describe('OwnerLessonsListItemComponent', () => {
  let component: OwnerLessonsListItemComponent;
  let fixture: ComponentFixture<OwnerLessonsListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnerLessonsListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnerLessonsListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

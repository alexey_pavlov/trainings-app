import { Component, ViewEncapsulation, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { get, isEmpty } from 'lodash';

import { IOwnerLesson } from '../../interfaces';

@Component({
    selector: 'app-lesson-form',
    templateUrl: './lesson-form.component.html',
    styleUrls: ['./lesson-form.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class LessonFormComponent implements OnInit {
    @Input() lesson: IOwnerLesson;
    @Output() save: EventEmitter<IOwnerLesson> = new EventEmitter();

    form: FormGroup;
    formSubmitted: boolean = false;

    constructor(private formBuilder: FormBuilder) {}

    ngOnInit() {
        this.form = this.formBuilder.group({
            title: [get(this.lesson, 'title'), Validators.compose([Validators.required, Validators.minLength(5)])],
            description: get(this.lesson, 'description'),
            text: get(this.lesson, 'text')
        });
    }

    submitForm() {
        this.formSubmitted = true;

        if (this.form.valid) {
            this.save.emit(Object.assign({}, this.lesson, this.form.value));
        }
    }

    get isNew(): boolean {
        return isEmpty(get(this.lesson, 'id'));
    }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { isEmpty } from 'lodash';

import { OwnerTrainingsService } from '../../services/owner-trainings.service';
import { IOwnerLesson, IOwnerTraining } from '../../interfaces';
import { newTrainingTempId } from '../../owner.constants';
import { IsDestroyedMixin } from '@shared/mixins/is-destroyed.mixin';
import { OwnerLessonsService } from '../../services/owner-lessons.service';


@Component({
    selector: 'app-owner-training-page',
    templateUrl: './owner-training.page.component.html',
    styleUrls: ['./owner-training.page.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class OwnerTrainingPageComponent extends IsDestroyedMixin implements OnInit {
    training$: Observable<IOwnerTraining>;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private ownerTrainingsService: OwnerTrainingsService,
        private ownerLessonsService: OwnerLessonsService
    ) {
        super();
    }

    ngOnInit() {
        this.training$ = this.activatedRoute.params
            .map(params => params.id)
            .switchMap((selectedTrainingId: string) => {
                if (!isEmpty(selectedTrainingId) && selectedTrainingId !== newTrainingTempId) {
                    return this.ownerTrainingsService.getOwnerTrainingById(selectedTrainingId);
                } else {
                    return Observable.of(null);
                }
            })
            .takeUntil(this.itIsDestroyed);
    }

    save(training: IOwnerTraining) {
        if (!training.id) {
            this.ownerTrainingsService.ownerTrainingCreatedSuccessfully$
                .take(1)
                .takeUntil(this.itIsDestroyed)
                .takeUntil(this.ownerTrainingsService.ownerTrainingCreatedFailure$)
                .subscribe((createdTraining: IOwnerTraining) => {
                    this.router.navigate(['/owner/trainings', createdTraining.id]);
                });

            this.ownerTrainingsService.createOwnerTraining(training);
        } else {
            this.ownerTrainingsService.updateOwnerTraining(training);
        }
    }

    deleteLesson(lesson: IOwnerLesson) {
        this.ownerLessonsService.deleteOwnerLesson(lesson);
    }
}

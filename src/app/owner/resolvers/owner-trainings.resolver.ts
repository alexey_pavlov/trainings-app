import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { OwnerTrainingsService } from '../services/owner-trainings.service';
import { IOwnerTraining } from '../interfaces';

@Injectable()
export class OwnerTrainingsResolver implements Resolve<IOwnerTraining[]> {
    constructor(private ownerTrainingsService: OwnerTrainingsService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<IOwnerTraining[]> {
        this.ownerTrainingsService.loadOwnerTrainings();

        return this.ownerTrainingsService.ownerTrainingsAfterLoading$.take(1);
    }
}

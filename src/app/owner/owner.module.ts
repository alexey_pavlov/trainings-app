import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// imports
import { SharedModule } from '@shared/shared.module';
import { OwnerRoutingModule } from './owner-routing.module';

// store
import { reducers, ownerModuleInitialState } from './state';
import { OwnerTrainingsEffects } from './state/effects/owner-trainings.effects';
import { OwnerLessonsEffects } from './state/effects/owner-lessons.effects';

// services
import { OwnerTrainingsApiService } from './services/owner-trainings.api.service';
import { OwnerTrainingsService } from './services/owner-trainings.service';
import { OwnerTrainingsResolver } from './resolvers/owner-trainings.resolver';
import { OwnerLessonsService } from './services/owner-lessons.service';
import { OwnerLessonsApiService } from './services/owner-lessons.api.service';

// components
import { OwnerTrainingsPageComponent } from './components/owner-trainings.page/owner-trainings.page.component';
import { OwnerTrainingsListComponent } from './components/owner-trainings-list/owner-trainings-list.component';
import { OwnerTrainingsListItemComponent } from './components/owner-trainings-list-item/owner-trainings-list-item.component';
import { OwnerTrainingPageComponent } from './components/owner-training.page/owner-training.page.component';
import { TrainingFormComponent } from './components/training-form/training-form.component';
import { LessonFormComponent } from './components/lesson-form/lesson-form.component';
import { OwnerLessonPageComponent } from './components/owner-lesson.page/owner-lesson.page.component';
import { OwnerLessonsListComponent } from './components/owner-lessons-list/owner-lessons-list.component';
import { OwnerLessonsListItemComponent } from './components/owner-lessons-list-item/owner-lessons-list-item.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        StoreModule.forFeature('OwnerModule', reducers, { initialState: ownerModuleInitialState }),
        EffectsModule.forFeature([
            OwnerTrainingsEffects,
            OwnerLessonsEffects
        ]),
        OwnerRoutingModule,
        SharedModule
    ],
    declarations: [
        OwnerTrainingsPageComponent,
        OwnerTrainingsListComponent,
        OwnerTrainingsListItemComponent,
        OwnerTrainingPageComponent,
        TrainingFormComponent,
        LessonFormComponent,
        OwnerLessonPageComponent,
        OwnerLessonsListComponent,
        OwnerLessonsListItemComponent
    ],
    providers: [
        OwnerTrainingsApiService,
        OwnerTrainingsService,
        OwnerTrainingsResolver,
        OwnerLessonsService,
        OwnerLessonsApiService
    ]
})
export class OwnerModule {}

export interface IOwnerLesson {
    id?: string;
    title: string;
    description?: string;
    text?: string;
    trainingId: string;
    ownerId: string;
}

import { IOwnerLesson } from './owner-lesson.interface';

export interface IOwnerTraining {
    id?: string;
    title: string;
    description?: string;
    text?: string;
    parentId?: string;
    ownerId: string;
    lessons?: IOwnerLesson[];
}

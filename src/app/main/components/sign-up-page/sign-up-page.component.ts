import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { get } from 'lodash';

import { AuthService } from '@core/services/auth.service';
import { IsDestroyedMixin } from '@shared/mixins/is-destroyed.mixin';
import { MessagesService } from '@core/services/messages.service';
import { customEmailValidator } from '@shared/validators/custom-email.validator';
import { IUserType } from '@user/interfaces';
import { studentUserType, ownerUserType } from '@user/user.constants';

@Component({
    selector: 'app-login-page',
    templateUrl: './sign-up-page.component.html',
    styleUrls: ['./sign-up-page.component.scss']
})
export class SignUpPageComponent extends IsDestroyedMixin implements OnInit {
    form: FormGroup;
    formSubmitted: boolean = false;
    isLoading: boolean = false;

    userTypes: [{ id: IUserType, label: string }] = [{
        id: studentUserType,
        label: 'common.userTypes.student'
    }, {
        id: ownerUserType,
        label: 'common.userTypes.owner'
    }];

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private messagesService: MessagesService
    ) {
        super();
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, customEmailValidator, Validators.minLength(6)])],
            firstName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
            lastName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
            type: [this.userTypes[0].id, Validators.required]
        });
    }

    signUp() {
        this.formSubmitted = true;

        if (this.form.valid) {
            this.authService.signUp(this.form.value);

            this.isLoading = true;

            this.authService.signedUpFailure$
                .take(1)
                .takeUntil(this.itIsDestroyed)
                .subscribe((errorResponse: HttpErrorResponse) => {
                    this.isLoading = false;

                    if (get(errorResponse, 'error.errors.email.unique')) {
                        this.messagesService.showMessage('errors.signUp.emailIsNotUnique', 'DANGER', true);
                    } else {
                        this.messagesService.showMessage('errors.default', 'DANGER', true);
                    }
                });
        }
    }
}

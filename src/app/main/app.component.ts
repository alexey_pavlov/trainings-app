import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'lodash';

import { AuthService } from '@core/services/auth.service';
import { LanguageService } from '@core/services/language.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    isAuthorised$: Observable<boolean> = this.authService.isAuthorised$;

    constructor(
        private authService: AuthService,
        private translateService: TranslateService,
        private languageService: LanguageService
    ) {}

    ngOnInit() {
        this.translateService.addLangs(<string[]>map(this.languageService.languages, 'language'));
        this.translateService.setDefaultLang(this.languageService.languages[0].language);
        this.translateService.use(this.languageService.language);
        this.authService.checkInitialState();
    }
}

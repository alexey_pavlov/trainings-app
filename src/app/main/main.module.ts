import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';

// modules
import { SharedModule } from '@shared/shared.module';
import { CoreModule } from '@core/core.module';
import { UserModule } from '@user/user.module';
import { TrainingsModule } from '@trainings/trainings.module';
import { DashboardModule } from '../dashboard/dashboard.module';
import { MainRoutingModule } from './main-routing.module';

// components
import { AppComponent } from './app.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { SignUpPageComponent } from './components/sign-up-page/sign-up-page.component';

// providers
import { TranslateHttpLoader } from '@core/services/translate-loader.service';

export function TranslateHttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}

@NgModule({
    imports: [
        BrowserModule,
        SharedModule,
        MainRoutingModule,
        CoreModule.forRoot(),
        UserModule.forRoot(),
        DashboardModule.forRoot(),
        TrainingsModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: TranslateHttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ],
    declarations: [
        AppComponent,
        LoginPageComponent,
        SignUpPageComponent
    ],
    bootstrap: [AppComponent]
})
export class MainModule {}

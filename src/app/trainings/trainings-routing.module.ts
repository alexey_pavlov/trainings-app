import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// providers
import { AuthGuard } from '@core/guards/auth.guard';
import { StudentGuard } from '@core/guards/student.guard';

// components
import { TrainingsPageComponent } from './components/trainings.page/trainings.page.component';

const routes: Routes = [
    {
        path: `trainings`,
        component: TrainingsPageComponent,
        canActivate: [AuthGuard, StudentGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TrainingsRoutingModule {}

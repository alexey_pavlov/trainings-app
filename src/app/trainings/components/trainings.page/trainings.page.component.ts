import { Component } from '@angular/core';

@Component({
    selector: 'app-trainings-page',
    templateUrl: './trainings.page.component.html',
    styleUrls: ['./trainings.page.component.scss']
})
export class TrainingsPageComponent {}

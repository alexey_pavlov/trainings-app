import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrainingsRoutingModule } from './trainings-routing.module';
import { TrainingsPageComponent } from './components/trainings.page/trainings.page.component';

@NgModule({
    imports: [
        CommonModule,
        TrainingsRoutingModule
    ],
    declarations: [
        TrainingsPageComponent
    ]
})
export class TrainingsModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: TrainingsModule,
            providers: []
        };
    }
}

import { ValidatorFn, AbstractControl } from '@angular/forms';
import isEmail from 'validator/lib/isEmail';

export const customEmailValidator: ValidatorFn = (control: AbstractControl) => {
    return !isEmail(control.value) ? { 'email': true } : null;
};

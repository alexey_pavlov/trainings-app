import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { QuillModule } from 'ngx-quill';

import { NavbarComponent } from './components/navbar/navbar.component';
import { EditorComponent } from './components/editor/editor.component';
import { SanitizedHtmlContainerComponent } from './components/sanitized-html-container/sanitized-html-container.component';

const components = [
    NavbarComponent,
    EditorComponent,
    SanitizedHtmlContainerComponent
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        NgxErrorsModule,
        QuillModule
    ],
    declarations: [...components, SanitizedHtmlContainerComponent],
    exports: [
        ...components,
        NgxErrorsModule,
        TranslateModule
    ]
})
export class SharedModule {}

import { Component, ViewEncapsulation, forwardRef, OnInit, AfterViewInit, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormControl } from '@angular/forms';
import { isUndefined } from 'lodash';
import { QuillEditorComponent } from 'ngx-quill';

import { IsDestroyedMixin } from '@shared/mixins/is-destroyed.mixin';

@Component({
    selector: 'app-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => EditorComponent),
            multi: true
        }
    ]
})
export class EditorComponent extends IsDestroyedMixin implements ControlValueAccessor, OnInit, AfterViewInit {
    @ViewChild(QuillEditorComponent) quillEditorComponent: QuillEditorComponent;

    model: string;
    htmlAreaControl: FormControl;
    htmlAreaControlIsShown: boolean = false;

    modules = {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            // toggled buttons
            ['blockquote', 'code-block'],
            [{ 'header': 1 }, { 'header': 2 }],
            // custom button values
            [{ 'list': 'ordered' }, { 'list': 'bullet' }],
            [{ 'script': 'sub' }, { 'script': 'super' }],
            // superscript/subscript
            [{ 'indent': '-1' }, { 'indent': '+1' }],
            // outdent/indent
            [{ 'direction': 'rtl' }],
            // text direction
            [{ 'size': ['small', false, 'large', 'huge'] }],
            // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'color': [] }, { 'background': [] }],
            // dropdown with defaults from theme
            [{ 'font': [] }],
            [{ 'align': [] }],
            ['clean'],
            // remove formatting button
            ['link', 'image', 'video'],
            // link and image, video,
            ['showHtml']
        ]
    };

    constructor(
        private elementRef: ElementRef,
        private renderer: Renderer2
    ) {
        super();
    }

    propagateChange = (_: any) => {};
    registerOnTouched() {}

    writeValue(value: any) {
        this.model = value;
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    ngOnInit() {
        this.htmlAreaControl = new FormControl();
    }

    ngAfterViewInit() {
        const customButton = this.elementRef.nativeElement.querySelector('.ql-showHtml');
        const editorArea = this.elementRef.nativeElement.querySelector('.ql-editor');

        customButton.addEventListener('click', () => {
            const html = this.htmlAreaControl.value;

            if (this.htmlAreaControlIsShown) {
                if (!isUndefined(html)) {
                    this.quillEditorComponent.quillEditor.pasteHTML(html);
                }
            } else {
                this.htmlAreaControl.patchValue(this.model);
            }

            this.htmlAreaControlIsShown = !this.htmlAreaControlIsShown;
            this.renderer.setStyle(editorArea, 'display', this.htmlAreaControlIsShown ? 'none' : 'block');
        });
    }

    contentChanged(data) {
        this.propagateChange(data.html);

        this.model = data.html;
    }
}

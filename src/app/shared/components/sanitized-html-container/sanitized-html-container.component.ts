import { Component, ViewEncapsulation, OnChanges, SimpleChanges, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { get } from 'lodash';

@Component({
    selector: 'app-sanitized-html-container',
    templateUrl: './sanitized-html-container.component.html',
    encapsulation: ViewEncapsulation.None
})
export class SanitizedHtmlContainerComponent implements OnChanges {
    @Input() htmlString: string;

    trustedHtml: SafeHtml;

    constructor(private domSanitizer: DomSanitizer) {}

    ngOnChanges(changes: SimpleChanges) {
        const htmlString = <string>get(changes, 'htmlString.currentValue');

        this.trustedHtml = this.domSanitizer.bypassSecurityTrustHtml(htmlString);
    }
}

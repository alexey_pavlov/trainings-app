import { IUserType } from '@user/interfaces';

export const studentUserType: IUserType = 'STUDENT';
export const ownerUserType: IUserType = 'OWNER';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { getBaseUrlUtil } from '../utilities/get-base-url.util';
const ruJson = require('../../../i18n/ru.json');

export class TranslateHttpLoader {
    constructor(private http: HttpClient) {}

    getTranslation(lang: string) {
        if (lang === 'ru') {
            return Observable.of(ruJson);
        } else {
            return this.getTranslationLazy(lang);
        }
    }

    getTranslationLazy(lang: string) {
        return this.http.get(`${getBaseUrlUtil()}/i18n/${lang}.json`);
    }
}

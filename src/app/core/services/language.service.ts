import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { isEmpty, keyBy, cloneDeep } from 'lodash';

interface Language {
    language: string;
    locale: string;
}

const LANGUAGES: Language[] = [{
    language: 'ru',
    locale: 'ru-RU'
}, {
    language: 'en',
    locale: 'en-US'
}];

const LANGUAGES_DICT = keyBy(LANGUAGES, 'language');

@Injectable()
export class LanguageService {
    languages: Language[] = cloneDeep(LANGUAGES);
    currentLanguage: Language;

    constructor(private localStorageService: LocalStorageService) {
        const languageFromLocalStorage: string = <string>this.localStorageService.get('language');

        this.currentLanguage = !isEmpty(languageFromLocalStorage) && !isEmpty(LANGUAGES_DICT[languageFromLocalStorage]) ? LANGUAGES_DICT[languageFromLocalStorage] : LANGUAGES[0];
    }

    get language(): string {
        return this.currentLanguage.language;
    }

    get locale(): string {
        return this.currentLanguage.locale;
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { IAuthSignInResponse, IAuthSignInRequest, IAuthSignUpRequest } from '../interfaces';
import { IUser } from '@user/interfaces';

@Injectable()
export class AuthApiService {
    constructor(private http: HttpClient) {}

    signIn(credentials: IAuthSignInRequest): Observable<IAuthSignInResponse> {
        return this.http.post<IAuthSignInResponse>('/auth/signin', credentials);
    }

    signUp(user: IAuthSignUpRequest): Observable<IUser> {
        return this.http.post<IUser>('/auth/signup', user);
    }
}

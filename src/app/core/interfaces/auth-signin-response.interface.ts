export interface IAuthSignInResponse {
    token: string;
}
